#!/usr/bin/env bash
#Remove duplicates
cat ~/.surf/history.txt > ~/.surf/history.txt.$$
cat ~/.surf/history.txt.$$ | sort | uniq >~/.surf/history.txt
rm -f ~/.surf/history.txt.$$
# show dmenu
tac ~/.surf/history.txt| dmenu -l 10 -b -i | cut -d ' ' -f 3
