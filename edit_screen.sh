#!/bin/sh
SUBLIME='subl3'
if $(which $SUBLIME); then 
	EDITOR=$SUBLIME
fi
tmpfile=$(mktemp /tmp/st-edit.XXXXXX)
if [ $EDITOR -ne $SUBLIME ]; then
	trap  'rm "$tmpfile"' 0 1 15 #remove temporary file
fi
cat > "$tmpfile"
st -e $EDITOR -n "$tmpfile"
