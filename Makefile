# surf - simple browser
# See LICENSE file for copyright and license details.
.POSIX:

include config.mk

SRC = surf.c
WSRC = webext-surf.c
OBJ = $(SRC:.c=.o)
WOBJ = $(WSRC:.c=.o)
WLIB = $(WSRC:.c=.so)

all: options surf $(WLIB)

options:
	@echo surf build options:
	@echo "CC            = $(CC)"
	@echo "CFLAGS        = $(SURFCFLAGS) $(CFLAGS)"
	@echo "WEBEXTCFLAGS  = $(WEBEXTCFLAGS) $(CFLAGS)"
	@echo "LDFLAGS       = $(LDFLAGS)"

surf: $(OBJ)
	$(CC) $(SURFLDFLAGS) $(LDFLAGS) -o $@ $(OBJ) $(LIBS)

$(OBJ) $(WOBJ): config.h common.h config.mk

config.h:
	cp config.def.h $@

$(OBJ): $(SRC)
	$(CC) $(SURFCFLAGS) $(CFLAGS) -c $(SRC)

$(WLIB): $(WOBJ)
	$(CC) -shared -Wl,-soname,$@ $(LDFLAGS) -o $@ $? $(WEBEXTLIBS)

$(WOBJ): $(WSRC)
	$(CC) $(WEBEXTCFLAGS) $(CFLAGS) -c $(WSRC)

clean:
	rm -f surf $(OBJ)
	rm -f $(WLIB) $(WOBJ)

distclean: clean
	rm -f config.h surf-$(VERSION).tar.gz

dist: distclean
	mkdir -p surf-$(VERSION)
	cp -R LICENSE Makefile config.mk config.def.h README \
	    surf-open.sh arg.h TODO.md surf.png \
	    surf.1 $(SRC) $(CSRC) $(WSRC) surf-$(VERSION)
	tar -cf surf-$(VERSION).tar surf-$(VERSION)
	gzip surf-$(VERSION).tar
	rm -rf surf-$(VERSION)

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f surf\
		edit_screen.sh\
		surf_history_dmenu.sh\
		surf_linkselect.sh\
		$(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/surf\
		$(DESTDIR)$(PREFIX)/bin/edit_screen.sh\
		$(DESTDIR)$(PREFIX)/bin/surf_history_dmenu.sh\
		$(DESTDIR)$(PREFIX)/bin/surf_linkselect.sh
	mkdir -p $(DESTDIR)$(LIBDIR)
	cp -f $(WLIB) $(DESTDIR)$(LIBDIR)
	mkdir -p /home/`users | head -n 1 | cut -d ' ' -f 1`/.surf/\
		/home/`users | head -n 1 | cut -d ' ' -f 1`/.surf/styles/
	cp -pf scripts/script.js \
		/home/`users | head -n 1 | cut -d ' ' -f 1`/.surf/
	cp -pf styles/default.black.css\
		styles/default.darkmode.css\
		styles/default.grey.css\
		styles/default.inverted.css\
		styles/default.lightly.inverted.css\
		styles/amazon.css\
		styles/bloomberg.css\
		styles/morningstar.css\
		styles/suckless.css\
		styles/teams.css\
		/home/`users | head -n 1 | cut -d ' ' -f 1`/.surf/styles/
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	sed "s/VERSION/$(VERSION)/g" < surf.1 > $(DESTDIR)$(MANPREFIX)/man1/surf.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/surf.1
	mkdir -p ${DESTDIR}${PREFIX}/share/surf
	cp -f surf.xb.mom ${DESTDIR}${PREFIX}/share/surf/surf.xb.mom
	chmod 644 $(DESTDIR)$(PREFIX)/share/surf/surf.xb.mom
	for wlib in $(WLIB); do \
	    chmod 644 $(DESTDIR)$(LIBDIR)/$$wlib; \
	done

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/surf\
		$(DESTDIR)$(PREFIX)/bin/edit_screen.sh\
		$(DESTDIR)$(PREFIX)/bin/surf_history_dmenu.sh\
		$(DESTDIR)$(PREFIX)/bin/surf_linkselect.sh\
		$(DESTDIR)$(MANPREFIX)/man1/surf.1
	cd /home/`users | head -n 1 | cut -d ' ' -f 1`/.surf/ && \
		rm -f ./script.js
	cd /home/`users | head -n 1 | cut -d ' ' -f 1`/.surf/styles/ && \
	rm -f default.black.css\
		default.darkmode.css\
		default.grey.css\
		default.inverted.css\
		default.lightly.inverted.css\
		amazon.css\
		bloomberg.css\
		morningstar.css\
		suckless.css\
		teams.css
	rm -f ${DESTDIR}${PREFIX}/share/surf/surf.xb.mom
	for wlib in $(WLIB); do \
	    rm -f $(DESTDIR)$(LIBDIR)/$$wlib; \
	done
#	- rmdir $(DESTDIR)$(LIBDIR)

.PHONY: all options distclean clean dist install uninstall
